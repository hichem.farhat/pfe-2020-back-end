package tn.essat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Compteépargne {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	@ManyToOne @JoinColumn(name="prodsou_id")
	private ProdSous prods;
	@ManyToOne @JoinColumn(name="compte_id")
	private Compte cp;
	
	private int num;
	@ManyToOne @JoinColumn(name="client_id")
	private Client cl;
	public Compteépargne() {
		super();
	}
	public Compteépargne(Integer id, ProdSous prods, Compte cp, int num, Client cl) {
		super();
		this.id = id;
		this.prods = prods;
		this.cp = cp;
		this.num = num;
		this.cl = cl;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ProdSous getProds() {
		return prods;
	}
	public void setProds(ProdSous prods) {
		this.prods = prods;
	}
	public Compte getCp() {
		return cp;
	}
	public void setCp(Compte cp) {
		this.cp = cp;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	
	

}
