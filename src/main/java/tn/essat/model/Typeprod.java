package tn.essat.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
@Entity
public class Typeprod implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private Integer id ;
	@Column(nullable = false)
	private String code;
	@Column(nullable = false)
	private String libelle;
	@JsonIgnore
	@OneToMany (cascade = CascadeType.ALL,mappedBy = "type")
	private List <Produit> liste;
	public Typeprod() {
		super();
	}
	
	public Typeprod(Integer id, String code, String libelle, List<Produit> liste) {
		super();
		this.id = id;
		this.code = code;
		this.libelle = libelle;
		this.liste = liste;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
