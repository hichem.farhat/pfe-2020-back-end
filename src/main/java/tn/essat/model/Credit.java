package tn.essat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Credit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne @JoinColumn(name="cl_id")
	private Client cl;
	@Column(nullable = false)
	private String montant;
	@ManyToOne @JoinColumn(name="cp_id")
	private Compte cp;
	private String validation ;
	private String date;
	
	public Credit() {
		super();
	}
	public Credit(Integer id, Client cl, String montant, Compte cp, String validation, String date) {
		super();
		this.id = id;
		this.cl = cl;
		this.montant = montant;
		this.cp = cp;
		this.validation = validation;
		this.date = date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	public String getMontant() {
		return montant;
	}
	public void setMontant(String montant) {
		this.montant = montant;
	}
	public Compte getCp() {
		return cp;
	}
	public void setCp(Compte cp) {
		this.cp = cp;
	}
	public String getValidation() {
		return validation;
	}
	public void setValidation(String validation) {
		this.validation = validation;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
