package tn.essat.model;

import java.io.Serializable;

public class prcent implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Integer id;
		Long pr;
		public prcent(Integer id,Long pr) {
			this.id=id;
			this.pr=pr;
		}
		@Override
		public String toString() {
			return "prcent [id=" + id + ", pr=" + pr + "]";
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Long getPr() {
			return pr;
		}
		public void setPr(Long pr) {
			this.pr = pr;
		}
		
	}