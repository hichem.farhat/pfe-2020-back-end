package tn.essat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Compte {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	@Column(nullable = false)
	private String code;
	@ManyToOne @JoinColumn(name="cl_id")
	private Client cl;
	private String libelle;
	private String date ;
	@ManyToOne @JoinColumn(name="typecompte_id")
	private TypeCompte cp;
	
	public Compte() {
		super();
	}
	
	public Compte(Integer id, String code, Client cl, String libelle, String date, TypeCompte cp) {
		super();
		this.id = id;
		this.code = code;
		this.cl = cl;
		this.libelle = libelle;
		this.date = date;
		this.cp = cp;
		
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public TypeCompte getCp() {
		return cp;
	}
	public void setCp(TypeCompte cp) {
		this.cp = cp;
	}
	
}