package tn.essat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ProdSous { 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	private String libelle;
	@ManyToOne @JoinColumn(name="sous_id")
	private Souscription sous;
	@ManyToOne @JoinColumn(name="prod_id")
	private Produit prod;
	
	private String date;
	public ProdSous() {
		super();
	}
	public ProdSous(Integer id, String libelle, Souscription sous, Produit prod, String date) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.sous = sous;
		this.prod = prod;
		this.date = date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Souscription getSous() {
		return sous;
	}
	
	public Produit getProd() {
		return prod;
	}
	public void setProd(Produit prod) {
		this.prod = prod;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setSous(Souscription sous) {
		this.sous = sous;
	}
	
	
}