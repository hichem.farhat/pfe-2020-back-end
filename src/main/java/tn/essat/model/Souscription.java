package tn.essat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Souscription {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String libelle;
	@ManyToOne @JoinColumn(name="prod_id")
	private Produit prod;
	@Column(nullable = false)
	private int numSouscription;
	@ManyToOne @JoinColumn(name="client_id")
	private Client cl;
	@ManyToOne @JoinColumn(name="compte_id")
	private Compte cp;
	@Column(nullable = false)
	private String date;
	private String etat;
	public Souscription() {
		super();
	}
	
	public Souscription(Integer id, String libelle, Produit prod, int numSouscription, Client cl, Compte cp,
			String date,String etat) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.prod = prod;
		this.numSouscription = numSouscription;
		this.cl = cl;
		this.cp = cp;
		this.date = date;
		this.etat=etat;
	}
	

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Produit getProd() {
		return prod;
	}
	public void setProd(Produit prod) {
		this.prod = prod;
	}
	public int getNumSouscription() {
		return numSouscription;
	}
	public void setNumSouscription(int numSouscription) {
		this.numSouscription = numSouscription;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	public Compte getCp() {
		return cp;
	}
	public void setCp(Compte cp) {
		this.cp = cp;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Souscription [id=" + id + ", libelle=" + libelle + ", prod=" + prod + ", numSouscription="
				+ numSouscription + ", cl=" + cl + ", cp=" + cp + ", date=" + date + "]";
	}

	
	

}
