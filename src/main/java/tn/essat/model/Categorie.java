package tn.essat.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Categorie implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private Integer id ;
	@Column(nullable = false)
	private String nom;
	  @JsonIgnore
	@OneToMany (cascade = CascadeType.ALL,mappedBy = "cat")
	private List <Client> liste;
	
	

	public Categorie(Integer id, String nom, List<Client> liste) {
		super();
		this.id = id;
		this.nom = nom;
		this.liste = liste;
	}
	public Categorie() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Client> getListe() {
		return liste;
	}
	public void setListe(List<Client> liste) {
		this.liste = liste;
	}
	

}
