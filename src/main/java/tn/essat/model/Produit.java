package tn.essat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

@Entity
public class Produit implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String codeprod;
	@Column(nullable = false)
	private String libelle;
	@ManyToOne @JoinColumn(name="type_id")
	private Typeprod type;
	private String date_de_creation;
	@ManyToOne @JoinColumn(name="car_id")
	private Caracteristique car;
	public Produit() {
		super();
	}
	public Produit(Integer id, String codeprod, String libelle, Typeprod type, String date_de_creation,
			Caracteristique car) {
		super();
		this.id = id;
		this.codeprod = codeprod;
		this.libelle = libelle;
		this.type = type;
		this.date_de_creation = date_de_creation;
		this.car = car;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodeprod() {
		return codeprod;
	}
	public void setCodeprod(String codeprod) {
		this.codeprod = codeprod;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Typeprod getType() {
		return type;
	}
	public void setType(Typeprod type) {
		this.type = type;
	}
	public String getDate_de_creation() {
		return date_de_creation;
	}
	public void setDate_de_creation(String date_de_creation) {
		this.date_de_creation = date_de_creation;
	}
	public Caracteristique getCar() {
		return car;
	}
	public void setCar(Caracteristique car) {
		this.car = car;
	}
	
	

}
