package tn.essat.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TypeCompte {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String libelle;
	@JsonIgnore
	@OneToMany (cascade = CascadeType.ALL,mappedBy = "cp")
	private List <Compte> liste;
	public TypeCompte() {
		super();
	}
	
	public TypeCompte(Integer id, String libelle, List<Compte> liste) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.liste = liste;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Compte> getListe() {
		return liste;
	}

	public void setListe(List<Compte> liste) {
		this.liste = liste;
	}
	

}
