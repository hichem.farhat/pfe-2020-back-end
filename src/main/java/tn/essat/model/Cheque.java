package tn.essat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Cheque {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne @JoinColumn(name="prods_id")
	private ProdSous prods;
	@ManyToOne @JoinColumn(name="client_id")
	private Client cl;
	public Cheque() {
		super();
	}
	public Cheque(Integer id, ProdSous prods, Client cl) {
		super();
		this.id = id;
		this.prods = prods;
		this.cl = cl;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ProdSous getProds() {
		return prods;
	}
	public void setProds(ProdSous prods) {
		this.prods = prods;
	}
	public Client getCl() {
		return cl;
	}
	public void setCl(Client cl) {
		this.cl = cl;
	}
	
	

}
