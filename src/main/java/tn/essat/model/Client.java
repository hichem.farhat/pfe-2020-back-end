package tn.essat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

@Entity
public class Client implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private String nom;
	@Column(nullable = false)
	private String adresse;

	@Column(nullable = false)
	private String cin;

	@Column(nullable = false)
	private String date_creation;

	@ManyToOne
	@JoinColumn(name = "cat_id")
	private Categorie cat;
	@Column(nullable = false)
	private Double salaire;
	@ManyToOne
	@JoinColumn(name = "ag_id")
	private Agence ag;

	public Client(Integer id, String nom, String adresse, String cin, String date_creation, Categorie cat,
			Double salaire, Agence ag) {
		super();
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.cin = cin;
		this.date_creation = date_creation;
		this.cat = cat;
		this.salaire = salaire;
		this.ag = ag;
	}

	public Agence getAg() {
		return ag;
	}

	public void setAg(Agence ag) {
		this.ag = ag;
	}

	public Double getSalaire() {
		return salaire;
	}

	public void setSalaire(Double salaire) {
		this.salaire = salaire;
	}

	public Client() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getDate_creation() {
		return date_creation;
	}

	public void setDate_creation(String date_creation) {
		this.date_creation = date_creation;
	}

	public Categorie getCat() {
		return cat;
	}

	public void setCat(Categorie cat) {
		this.cat = cat;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

}
