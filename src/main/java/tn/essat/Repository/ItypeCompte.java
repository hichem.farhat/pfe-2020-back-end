package tn.essat.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.TypeCompte;
@Repository
public interface ItypeCompte extends JpaRepository<TypeCompte, Integer> {

}
