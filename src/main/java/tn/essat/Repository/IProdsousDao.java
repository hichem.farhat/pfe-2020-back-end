package tn.essat.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.ProdSous;
@Repository
public interface IProdsousDao extends JpaRepository<ProdSous, Integer> {
	@Query("select count(ps) from ProdSous ps where ps.prod.id=:x")
	public Long cc(@Param("x") int x);
	@Query("select ps from ProdSous ps where ps.sous.cl.ag.id=:x")
	public List <ProdSous> getallProdsouscrirebyag(@Param("x") int x);
	@Query("select count(ps) from ProdSous ps where ps.prod.id=:x and ps.sous.cl.ag.id=:x2")
	public Long cc2(@Param("x") int x,@Param("x2") int x2);
	@Query("select count(ps) from ProdSous ps where ps.sous.cl.id=:x")
	public Long cccl(@Param("x") int x);
	@Query("select count(ps) from ProdSous ps where ps.sous.cl.ag.id=:x")
	public Long couuntag(@Param("x") int x);
	@Query("select count(ps) from ProdSous ps where ps.prod.id=:x and ps.sous.cl.ag.id=:x2")
	public Long countbyprodandag(@Param("x")int x ,@Param("x2")int x2);
	@Query("select count(ps) from ProdSous ps,Produit p where ps.prod.id=p.id ")
	public List <Long> test();
	@Query("select ps from ProdSous ps where ps.sous.id=:x")
	public Optional<ProdSous> getprodsouscrire(@Param("x") int x);
	
	
	
	
	
}
