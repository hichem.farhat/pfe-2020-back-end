package tn.essat.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Produit;
@Repository
public interface IProdDao extends JpaRepository<Produit, Integer>{
	@Query("select p from Produit p where p.car.id=:x")
	public List<Produit>getbycar(@Param("x") int x);
	//@Query("select   p  from Produit p where ")
	//public List<Long> getpppppp();
}
