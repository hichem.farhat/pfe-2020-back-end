package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Compte;
@Repository
public interface ICompteDao extends JpaRepository<Compte, Integer> {
	@Query("select c from Compte c where c.cl.ag.id=:x")
	public List <Compte> getallcomptebyag(@Param("x")int x);

}
