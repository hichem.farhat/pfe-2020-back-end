package tn.essat.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import tn.essat.model.Cheque;
@Repository
public interface Chequedao extends JpaRepository<Cheque, Integer>{
	@Query("select c from Cheque c where c.cl.ag.id=:x")
	public List <Cheque>getallChequeByagence(@Param("x")int id);
	@Query("select count(c) from Cheque c where c.cl.ag.id=:x")
	public Long countag(@Param("x") int x);
	@Query("select ch from Cheque ch where ch.prods.id=:x")
	public Optional <Cheque> getcheque(@Param("x") int x);

}
