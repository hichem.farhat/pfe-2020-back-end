package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Souscription;
@Repository
public interface ISousDao extends JpaRepository<Souscription, Integer> {
	@Query("select s from Souscription s where s.cl.ag.id=:x")
	public List <Souscription> getallsousbyagence(@Param("x")int id);
	@Query("select count(s) from Souscription s where s.cl.cat.id=:x")
	public Long countbycat(@Param("x")int id);
	@Query("select count(s) from Souscription s where s.cl.ag.id=:x")
	public Long countbyag(@Param("x")int id);
	@Query("select s from Souscription s where s.cl.ag.id=:x")
	public List<Souscription> getsousbyag(@Param("x")int id);
	@Query("select s from Souscription s where s.cl.cat.id=:x")
	public List<Souscription> getsousbycat(@Param("x")int id);
	@Query("select s from Souscription s where s.date BETWEEN :x1 AND :x2")
	public List<Souscription> getsouscriptionbydate(@Param("x1")String x,@Param("x2")String x2);
	
	@Query("select s from Souscription s where s.date=:x and s.cl.cat.id=:x2")
	public List<Souscription> getsouscriptionbydateandcat(@Param("x")String x,@Param("x2")int x2);
	@Query("select s from Souscription s where s.cl.ag.id=:x and s.date=:x2")
	public List<Souscription> getsousbyaganddate(@Param("x")int id,@Param("x2")String id2);
	@Query("select s from Souscription s where s.cl.ag.id=:x1 and s.cl.cat.id=:x2")
	public List<Souscription> getsousbyagandcat(@Param("x1")int id,@Param("x2")int id2);
	@Query("select count(s) from Souscription s where s.etat='valider'")
	public Long getsousbyetatvalider();
	@Query("select count(s) from Souscription s where s.etat='non valider'")
	public Long getsousbyetatnonvalider();
	@Query("select s from Souscription s where s.etat='valider'")
	public List<Souscription> souscriptionvalider();
	@Query("select s from Souscription s where s.etat='non valider'")
	public List<Souscription> souscriptionnonvalider();
	@Query("select s from Souscription s where s.etat='non valider' AND  s.cl.ag.id=:x")
	public List<Souscription> souscriptionnonvaliderag(@Param("x") int id);
	@Query("select s from Souscription s where s.etat='valider' AND s.cl.ag.id=:x")
	public List<Souscription> souscriptionvaliderag(@Param("x") int id);
	@Query("select s from Souscription s where s.date BETWEEN :x1 AND :x2 AND s.cl.ag.id=:x3")
	public List<Souscription> souscriptiondateag(@Param("x1") String date1,@Param("x2") String date2,@Param("x3")int id);
	
	@Query("select s from Souscription s where s.cl.cat.id=:x and s.date BETWEEN :x1 AND :x2")
	public List<Souscription> getsousbycatanddate(@Param("x")int id,@Param("x1")String d1,@Param("x2")String d2);
	
}
