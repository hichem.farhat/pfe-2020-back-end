package tn.essat.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Agence;
import tn.essat.model.User;

@Repository
public interface IUserDao extends JpaRepository<User, Integer> {
	public User findByUsername(String username);
	@Query("select u from User u where u.role='USER'")
	public List <User> getus();
//@Modifying
	//@Query("update User u set u.username = :x1  and set u.name=:x3  and set u.ag=:x2 where u.id = :id")
	//void updateUser(@Param("x1") String username, @Param("x2")Agence ag,
	  //@Param("x3")String name,@Param("id")int id);

}
