package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Banqueadistance;
import tn.essat.model.Compte_a_vue;
@Repository
public interface ICompte_a_vue extends JpaRepository<Compte_a_vue, Integer> {
	@Query("select c from Compte_a_vue c where c.cl.ag.id=:x")
	public List <Compte_a_vue>getallCompte_a_vueByagence(@Param("x")int id);
	@Query("select count(c) from Compte_a_vue c where c.cl.ag.id=:x")
	public Long countag(@Param("x") int x);

}
