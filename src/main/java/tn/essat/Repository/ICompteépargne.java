package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Compteépargne;
@Repository
public interface ICompteépargne extends JpaRepository<Compteépargne, Integer> {
	@Query("select c from Compteépargne c where c.cl.ag.id=:x")
	public List <Compteépargne>getallCompteépargneByagence(@Param("x")int id);
	@Query("select count(c) from Compteépargne c where c.cl.ag.id=:x")
	public Long countag(@Param("x") int x);

}
