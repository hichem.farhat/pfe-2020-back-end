package tn.essat.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.Categorie;

@Repository
public interface ICatDao extends CrudRepository<Categorie, Integer> {

}
