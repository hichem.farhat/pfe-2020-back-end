package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Client;

@Repository
public interface IClientDao extends CrudRepository<Client, Integer>{
	@Query("select c from Client c where c.cat.id=:x")
	public List<Client> getallClientBycat(@Param("x")int x);
	@Query("select c from Client c where c.ag.id=:x")
	public List <Client>getallclientByagence(@Param("x")int id);
	@Query("select count(c) from Client c where c.cat.id=:x")
	public Long countt(@Param("x")int x);
	@Query("select count(c) from Client c where c.ag.id=:x")
	public Long counttag(@Param("x")int x);
	@Query("select count(c) from Client c where c.ag.id=:x and c.cat.id=:x2 ")
	public Long counttbyagandcat(@Param("x")int x,@Param("x2")int x2);
	


}
