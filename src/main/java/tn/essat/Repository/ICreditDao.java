package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Credit;
@Repository
public interface ICreditDao extends JpaRepository<Credit, Integer> {
	List<Credit> findByValidation(String validation);
	@Query("select count(c) from Credit c where c.validation='oui'")
	public long countui();
	@Query("select count(c) from Credit c where c.validation='non'")
	public long countnon();
	@Query("select c from Credit c where c.cl.ag.id=:x")
	public List<Credit> getallcreditbyagence(@Param("x")int id);
	@Query("select count(c) from Credit c where c.cl.ag.id=:x")
	public long countag(@Param("x")int id);
	@Query("select count(c) from Credit c where c.cl.ag.id=:x and c.validation='oui'")
	public long countagandvalider(@Param("x")int id);
	@Query("select count(c) from Credit c where c.cl.ag.id=:x and c.validation='non'")
	public long countagandnonvalider(@Param("x")int id);

}
