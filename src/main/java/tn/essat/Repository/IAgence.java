package tn.essat.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.Agence;

@Repository
public interface IAgence extends JpaRepository<Agence, Integer> {
	

}
