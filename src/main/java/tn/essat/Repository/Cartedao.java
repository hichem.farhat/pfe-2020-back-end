package tn.essat.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Carte;

@Repository
public interface Cartedao extends JpaRepository<Carte, Integer> {
	@Query("select c from Carte c where c.cl.ag.id=:x")
	public List <Carte>getallcarteByagence(@Param("x")int id);
	@Query("select count(c) from Carte c where c.cl.ag.id=:x")
	public Long countag(@Param("x") int x);
	@Query("select c from Carte c where c.prods.id=:x")
	public Optional <Carte> getcarte(@Param("x") int x);

}
