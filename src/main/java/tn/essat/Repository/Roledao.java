package tn.essat.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tn.essat.model.Rolee;

public interface Roledao extends CrudRepository<Rolee, Integer> {
	@Query("select r from Rolee r where r.type='USER'")
	public Rolee findrolebytype();

}
