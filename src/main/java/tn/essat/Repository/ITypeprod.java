package tn.essat.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import tn.essat.model.Typeprod;
@Repository
public interface ITypeprod extends JpaRepository<Typeprod, Integer> {

}
