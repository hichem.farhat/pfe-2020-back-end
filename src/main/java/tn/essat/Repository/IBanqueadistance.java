package tn.essat.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.essat.model.Banqueadistance;
import tn.essat.model.Cheque;
@Repository
public interface IBanqueadistance extends JpaRepository<Banqueadistance, Integer>{
	@Query("select b from Banqueadistance b where b.cl.ag.id=:x")
	public List <Banqueadistance>getallBanqueadistanceByagence(@Param("x")int id);
	@Query("select count(b) from Banqueadistance b where b.cl.ag.id=:x")
	public Long countag(@Param("x") int x);

}
