package tn.essat.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import tn.essat.model.Typeprod;



public class PDFGenerator {

	  private static Logger logger = LoggerFactory.getLogger(PDFGenerator.class);
		  
		  public static ByteArrayInputStream customerPDFReport(List<Typeprod> types) {
		    Document document = new Document();
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		        
		        try {
		          
		          PdfWriter.getInstance(document, out);
		            document.open();
		          
		            // Add Text to PDF file ->
		          Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
		          Paragraph para = new Paragraph( "types produits disponibles", font);
		          para.setAlignment(Element.ALIGN_CENTER);
		          document.add(para);
		          document.add(Chunk.NEWLINE);
		          
		          PdfPTable table = new PdfPTable(3);
		          // Add PDF Table Header ->
		            Stream.of("ID", "code", "libelle")
		              .forEach(headerTitle -> {
		                  PdfPCell header = new PdfPCell();
		                  Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		                  header.setBackgroundColor(BaseColor.LIGHT_GRAY);
		                  header.setHorizontalAlignment(Element.ALIGN_CENTER);
		                  header.setBorderWidth(2);
		                  header.setPhrase(new Phrase(headerTitle, headFont));
		                  table.addCell(header);
		              });
		            
		            for (Typeprod typer : types) {
		              PdfPCell idCell = new PdfPCell(new Phrase(typer.getId().toString()));
		              idCell.setPaddingLeft(4);
		              idCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		              idCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		                table.addCell(idCell);
		 
		                PdfPCell codeCell = new PdfPCell(new Phrase(typer.getCode()));
		                codeCell.setPaddingLeft(4);
		                codeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		                codeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		                table.addCell(codeCell);
		 
		                PdfPCell libellecell = new PdfPCell(new Phrase(String.valueOf(typer.getLibelle())));
		                libellecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		                libellecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		                libellecell.setPaddingRight(4);
		                table.addCell(libellecell);
		            }
		            document.add(table);
		            
		            document.close();
		        }catch(DocumentException e) {
		          logger.error(e.toString());
		        }
		        
		    return new ByteArrayInputStream(out.toByteArray());
		  }
		}

