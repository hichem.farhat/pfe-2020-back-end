package tn.essat.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import tn.essat.Repository.IProdDao;
import tn.essat.Repository.IProdsousDao;
import tn.essat.model.ProdSous;
import tn.essat.model.Produit;
import tn.essat.model.prcent;


@CrossOrigin("*")
@RestController
@RequestMapping("/prodsous")
public class RestProdSous {
	@Autowired
	IProdsousDao prodsous;
	@Autowired
	IProdDao prodDao;

	public void setProdDao(IProdDao prodDao) {
		this.prodDao = prodDao;
	}
	public void setProdsous(IProdsousDao prodsous) {
		this.prodsous = prodsous;
	}
	@GetMapping("/prodsous")
	public List <ProdSous> getall(){
		return (List <ProdSous>)prodsous.findAll();
	}
	@PostMapping("save")
	public void add(@RequestBody ProdSous pr) {
		prodsous.save(pr);	
		
	}
	@GetMapping("/getprodsouscrire/{id}")
	public Optional<ProdSous> getprodsouscrire(@PathVariable("id") int id){
		return prodsous.getprodsouscrire(id);}
	
	
	@GetMapping("count")
	public Long count(){
		return prodsous.count();
	}
	@DeleteMapping("/delprodsous/{id}")
	public void delclt(@PathVariable("id") int id) {
		prodsous.deleteById(id);;
	}
	@GetMapping("/getprodsous/{id}")
	public Optional<ProdSous> getpds(@PathVariable("id") int id) {
		
		return prodsous.findById(id);
	}
	@PutMapping("/updateprodsous/{id}")
	 
	  public ResponseEntity<ProdSous> updateprodsous(@PathVariable("id") int id, @RequestBody ProdSous Prodsou) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<ProdSous> prodsData = prodsous.findById(id);
	    if (prodsData.isPresent()) {
	    	ProdSous prodss = prodsData.get();
	    	prodss.setLibelle(Prodsou.getLibelle());
	    	prodss.setDate(Prodsou.getDate());
	    	prodss.setSous(Prodsou.getSous());
	    	
	  
	      
	     return new ResponseEntity<>(prodsous.save(prodss), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	
	
	@GetMapping("/cc/{id}")
	public Long cc(@PathVariable ("id")int x)
	{return prodsous.cc(x);}
	@GetMapping("/ff")
	List<prcent> porcentage(){
		List<Produit> prcentagesP=prodDao.findAll();
		List<prcent> prcentages=new ArrayList<prcent>();
		for (Produit p : prcentagesP) {
			prcent pr = new prcent(p.getId(), this.cc(p.getId())*100/this.count());
			System.out.println(pr.toString());
			prcentages.add(pr);
			System.out.println(prcentages.toString());
		}
		System.out.println(prcentages.toString());
		return prcentages;
		
	}
	@GetMapping("/psag/{id}")
	public List <ProdSous> getallProdsouscrirebyag(@PathVariable("id") int id){
		return (List <ProdSous> ) prodsous.getallProdsouscrirebyag(id);}
	@GetMapping("/cc2/{id}/{id}")
	public Long cc2(@PathVariable("id") int id,@PathVariable("id") int id2) {
		return prodsous.cc2(id, id2);}
	@GetMapping("/ff2/{id}")
	List<prcent> porcentageag(@PathVariable("id") int x){
		List<Produit> prcentagesP=prodDao.findAll();
		List<prcent> prcentages=new ArrayList<prcent>();
		for (Produit p : prcentagesP) {
			prcent pr = new prcent(p.getId(), countbyprodandag(p.getId(),x)*100/this.couuntag(x));
			System.out.println(pr.toString());
			prcentages.add(pr);
			System.out.println(prcentages.toString());
		}
		System.out.println(prcentages.toString());
		return prcentages;
		
	}
	@GetMapping("/cccl/{id}")
	public Long cccl(@PathVariable("id") int x) {
		return prodsous.cccl(x);}
	@GetMapping("/couuntag/{id}")
	public Long couuntag(@PathVariable("id")int x) {
		return prodsous.couuntag(x);}
	@GetMapping("/countbyprodandag/{id}/{id2}")
	public Long countbyprodandag(@PathVariable("id") int x1,@PathVariable("id2") int x2) {
		return prodsous.countbyprodandag(x1, x2);}
	@GetMapping("/test")
	public List <Long> test(){
		return (List<Long>)prodsous.test();
	}
	

}
