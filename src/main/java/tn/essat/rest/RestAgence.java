package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import tn.essat.Repository.IAgence;
import tn.essat.model.Agence;



@CrossOrigin("*")
@RestController
@RequestMapping("/ag")

public class RestAgence {
	@Autowired
	private IAgence ag;

	public void setAg(IAgence ag) {
		this.ag = ag;
	}
	 
	@GetMapping("/agences")
	public List<Agence>getall(){
		return ( List<Agence>)ag.findAll();}
	@PostMapping("/save")
	public void save(@RequestBody Agence a){
		ag.save(a);
		
		
		
	}
	@GetMapping("/count")
	public Long count() {
		return ag.count();
		
		
	}
	@DeleteMapping("/delag/{id}")
	public void del(@PathVariable("id") int id){
		ag.deleteById(id);
		
		
		
	}
	@GetMapping("/ag/{id}")
	public Optional<Agence>getagencebyid(@PathVariable("id") int id){
		
		return ag.findById(id);
		
	}
	@PutMapping("/updateag/{id}")
	 
	  public ResponseEntity<Agence> updateUclt(@PathVariable("id") int id, @RequestBody Agence Ag) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Agence> AgData = ag.findById(id);
	    if (AgData.isPresent()) {
	    	Agence agg = AgData.get();
	   agg.setLocale(Ag.getLocale());
	   agg.setLibelle(Ag.getLibelle());
	      
	     return new ResponseEntity<>(ag.save(agg), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	
	
	

}
