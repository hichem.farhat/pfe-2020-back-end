package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.Chequedao;
import tn.essat.model.Carte;
import tn.essat.model.Cheque;
@CrossOrigin("*")
@RestController
@RequestMapping("/cheque")
public class Restcheque {
	@Autowired
	Chequedao cheque;

	public void setCheque(Chequedao cheque) {
		this.cheque = cheque;
	}
	@GetMapping("/cheques")
	public List <Cheque> getall(){
		return (List <Cheque>) cheque.findAll();}
	@DeleteMapping("/delcheque/{id}")
	public void delcheque(@PathVariable("id") int id) {
		cheque.deleteById(id);
	}
	@PostMapping("/addcheque")
	public void addcheque(@RequestBody Cheque c) {
		cheque.save(c);
	}
	@GetMapping("/count")
	public Long getcount() {
		return cheque.count();
	}
	@GetMapping("/chequeag/{id}")
	public List <Cheque> getallbyag(@PathVariable ("id") int id){
		return (List <Cheque>) cheque.getallChequeByagence(id) ;
	}
	@GetMapping("/countag/{id}")
	public Long countag(@PathVariable("id") int id) {
		return cheque.countag(id);
	}
	@GetMapping("/getcheque/{id}")
	public Optional<Cheque> getcheque(@PathVariable("id") int x){
		return cheque.getcheque(x);
		
	}
	
	
	

}
