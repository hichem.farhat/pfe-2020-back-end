package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.Cartedao;
import tn.essat.model.Carte;
import tn.essat.model.Compte_a_vue;


@CrossOrigin("*")
@RestController
@RequestMapping("/carte")
public class Restcarte {
	@Autowired
	Cartedao carte;

	public void setCarte(Cartedao carte) {
		this.carte = carte;
	}
	@GetMapping("/cartes")
	public List<Carte>getall(){
		return (List<Carte>) carte.findAll();
		
		
	}
	@DeleteMapping("/delcarte/{id}")
	public void delcarte(@PathVariable("id") int id) {
		carte.deleteById(id);
	}
	public Long count() {
		return carte.count();
	}
	@PostMapping("/addcarte")
	public void addcat(@RequestBody Carte c) {
		carte.save(c);
	}
	@GetMapping("/count")
	public Long getcount() {
		return carte.count();
	}
	@GetMapping("/carteag/{id}")
	public List <Carte> getallbyag(@PathVariable ("id") int id){
		return (List <Carte>) carte.getallcarteByagence(id);
	}
	@GetMapping("/countag/{id}")
	public Long countag(@PathVariable("id") int id) {
		return carte.countag(id);
	}
	@GetMapping("/getcarte/{id}")
	public Optional <Carte> getcarte(@PathVariable("id") int x){
		return carte.getcarte(x);}
	

}
