package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.Caracdao;

import tn.essat.model.Caracteristique;

@CrossOrigin("*")
@RestController
@RequestMapping("/carac")
public class CarcRest {
	@Autowired
private Caracdao carac;

	public void setCarac(Caracdao carac) {
		this.carac = carac;
	}
	
	@PostMapping("/save")
	public void save(@RequestBody Caracteristique c){
		carac.save(c);
		
		
		
	}
	@GetMapping("/cars")
	public List<Caracteristique>getall(){
		return ( List<Caracteristique>)carac.findAll();}
	@GetMapping("/count")
	public Long count() {
		return carac.count();
		
		
	}
	@DeleteMapping("/delca/{id}")
	public void del(@PathVariable("id") int id){
		carac.deleteById(id);
		
		
		
	}
	@GetMapping("/ca/{id}")
	public Optional<Caracteristique>getcaracebyid(@PathVariable("id") int id){
		
		return carac.findById(id);
		
	}
	@PutMapping("/updatecarac/{id}")
	 
	  public ResponseEntity<Caracteristique> updateUclt(@PathVariable("id") int id, @RequestBody Caracteristique Ca) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Caracteristique> CaData = carac.findById(id);
	    if (CaData.isPresent()) {
	    	Caracteristique caa = CaData.get();
	   caa.setLibelle(Ca.getLibelle());
	      
	     return new ResponseEntity<>(carac.save(caa), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
}
