package tn.essat.rest;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.Cartedao;
import tn.essat.Repository.Chequedao;
import tn.essat.Repository.IBanqueadistance;
import tn.essat.Repository.ICatDao;
import tn.essat.Repository.ICompte_a_vue;
import tn.essat.Repository.ICompteépargne;
import tn.essat.Repository.IProdsousDao;
import tn.essat.Repository.ISousDao;
import tn.essat.model.Banqueadistance;
import tn.essat.model.Carte;
import tn.essat.model.Categorie;
import tn.essat.model.Cheque;
import tn.essat.model.Compte_a_vue;
import tn.essat.model.Compteépargne;
import tn.essat.model.ProdSous;
import tn.essat.model.Souscription;

@CrossOrigin("*")
@RestController
@RequestMapping("/sous")
public class RestSous {
	@Autowired
	ISousDao sous;
	@Autowired
	IProdsousDao prodsous;
	@Autowired
	Cartedao carte;
	@Autowired
	Chequedao cheque;
	@Autowired
	IBanqueadistance bad;
	@Autowired
	ICompte_a_vue cav;
	@Autowired
	ICompteépargne Ce;
	@Autowired
	ICatDao cat;

	public void setCat(ICatDao cat) {
		this.cat = cat;
	}

	public void setCarte(Cartedao carte) {
		this.carte = carte;
	}

	public void setCheque(Chequedao cheque) {
		this.cheque = cheque;
	}

	public void setBad(IBanqueadistance bad) {
		this.bad = bad;
	}

	public void setCav(ICompte_a_vue cav) {
		this.cav = cav;
	}

	public void setCe(ICompteépargne ce) {
		Ce = ce;
	}

	public void setProdsous(IProdsousDao prodsous) {
		this.prodsous = prodsous;
	}

	public void setSous(ISousDao sous) {
		this.sous = sous;
	}

	@GetMapping("souss")
	List<Souscription> getall() {
		return (List<Souscription>) sous.findAll();

	}

	@PostMapping("add")
	public void save(@RequestBody Souscription s) {
		// Random rand = new Random();
		// int rand_int1 = rand.nextInt(19999);
		s.setEtat("non valider");
		sous.save(s);
		// ProdSous ps = new ProdSous();
		// Carte c = new Carte();
		// Cheque ch = new Cheque();
		// Compte_a_vue cpav = new Compte_a_vue();
		// Banqueadistance bq = new Banqueadistance();
		// Compteépargne cae = new Compteépargne();
		// ps.setLibelle(s.getLibelle());
		// ps.setProd(s.getProd());
		/**
		 * ps.setDate(s.getDate()); ps.setSous(s);
		 * 
		 * prodsous.save(ps); if
		 * (s.getProd().getType().getLibelle().equals("Souscription Package")) {
		 * c.setCl(s.getCl()); c.setCp(s.getCp()); c.setNum(rand_int1); c.setProds(ps);
		 * ch.setProds(ps); ch.setCl(s.getCl()); cheque.save(ch); carte.save(c); } if
		 * (s.getProd().getType().getLibelle().equals("compte epargne")) {
		 * cae.setCl(s.getCl()); cae.setCp(s.getCp()); cae.setNum(rand_int1);
		 * cae.setProds(ps); Ce.save(cae);
		 * 
		 * } if (s.getProd().getType().getLibelle().equals("compte a vue")) {
		 * cpav.setCl(s.getCl()); cpav.setCp(s.getCp()); cpav.setNum(rand_int1);
		 * cpav.setProds(ps); cav.save(cpav); } if
		 * (s.getProd().getType().getLibelle().equals("banque a distance")) {
		 * bq.setCl(s.getCl()); bq.setCp(s.getCp()); bq.setNum(rand_int1);
		 * bq.setProds(ps); bad.save(bq);
		 * 
		 * } if (s.getProd().getType().getLibelle().equals("carte")) {
		 * c.setCl(s.getCl()); c.setCp(s.getCp()); c.setNum(rand_int1); c.setProds(ps);
		 * carte.save(c);
		 * 
		 * } if (s.getProd().getType().getLibelle().equals("cheque")) {
		 * ch.setCl(s.getCl());
		 * 
		 * ch.setProds(ps); cheque.save(ch);
		 * 
		 * }
		 */

	}

	@GetMapping("/getsouscriptionbyid/{id}")
	public Optional<Souscription> getsouscriptionbyid(@PathVariable("id") int id) {
		return sous.findById(id);
	}

	@GetMapping("/getcountbyag/{id}")
	public Long getcountbyag(@PathVariable("id") int id) {
		return sous.countbyag(id);
	}

	@GetMapping("/count")
	public Long count() {
		return sous.count();
	}

	@DeleteMapping("/delsous/{id}")
	public void delclt(@PathVariable("id") int id) {
		sous.deleteById(id);
	
		
		
		
	}

	@GetMapping("/getsous/{id}")
	public Optional<Souscription> getsous(@PathVariable("id") int id) {
		return sous.findById(id);
	}
	@PutMapping("/refuser/{id}")
	public ResponseEntity<Souscription> refuser(@PathVariable("id") int id, @RequestBody Souscription Sous) {
		
		
		
		
		
		
		System.out.println("Update type ID = " + id + "...");
		Optional<Souscription> sousData = sous.findById(id);
		//Optional <ProdSous>  pss= prodsous.getprodsouscrire(id);
		
		if (sousData.isPresent() ) {
			Souscription souss = sousData.get();
			souss.setCl(souss.getCl());
			souss.setCp(souss.getCp());
			souss.setDate(souss.getDate());
			souss.setLibelle(souss.getLibelle());
			souss.setNumSouscription(souss.getNumSouscription());
			souss.setProd(souss.getProd());
			souss.setEtat("refusé");
			//Optional <Cheque>  ch= cheque.getcheque(ps.getId());
			//Optional <Carte>  crr= carte.getcarte(ps.getId());
			//if(souss.getEtat().equals("oui")) {
				//ps.setLibelle(souss.getLibelle());
				//ps.setDate(souss.getDate());
				//ps.setProd(souss.getProd());
				
			//}
			//Optional <Carte>  crr= carte.getcarte(ps.getId());
			//Carte  cc=crr.get();
			//if ((souss.getEtat().equals("oui")) && (souss.getProd().getType().getLibelle().equals("carte"))) {
			//	cc.setCl(souss.getCl());
			//	cc.setCp(souss.getCp());
				
				
				
			//}
			
			
			
			
			
			
			
		    
			
			
			
			

			return new ResponseEntity<>(sous.save(souss), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@PutMapping("/updatetsous/{id}")
	public ResponseEntity<Souscription> updateUclt(@PathVariable("id") int id, @RequestBody Souscription Sous) {
		
		
		
		
		
		
		System.out.println("Update type ID = " + id + "...");
		Optional<Souscription> sousData = sous.findById(id);
		//Optional <ProdSous>  pss= prodsous.getprodsouscrire(id);
		
		if (sousData.isPresent() ) {
			Souscription souss = sousData.get();
			souss.setCl(Sous.getCl());
			souss.setCp(Sous.getCp());
			souss.setDate(Sous.getDate());
			souss.setLibelle(Sous.getLibelle());
			souss.setNumSouscription(Sous.getNumSouscription());
			souss.setProd(Sous.getProd());
			//ProdSous ps=pss.get();
			//Optional <Cheque>  ch= cheque.getcheque(ps.getId());
			//Optional <Carte>  crr= carte.getcarte(ps.getId());
			//if(souss.getEtat().equals("oui")) {
				//ps.setLibelle(souss.getLibelle());
				//ps.setDate(souss.getDate());
				//ps.setProd(souss.getProd());
				
			//}
			//Optional <Carte>  crr= carte.getcarte(ps.getId());
			//Carte  cc=crr.get();
			//if ((souss.getEtat().equals("oui")) && (souss.getProd().getType().getLibelle().equals("carte"))) {
			//	cc.setCl(souss.getCl());
			//	cc.setCp(souss.getCp());
				
				
				
			//}
			
			
			
			
			
			
			
		    
			
			
			
			

			return new ResponseEntity<>(sous.save(souss), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/sousag/{id}")
	public List<Souscription> getallsousbyagence(@PathVariable("id") int id) {
		return (List<Souscription>) sous.getallsousbyagence(id);
	}

	@GetMapping("/countsousbycat/{id}")
	public Long countbycat(@PathVariable("id") int id) {
		return sous.countbycat(id);
	}

	@GetMapping("/getsousbyag/{id}")
	public List<Souscription> getsousbyag(@PathVariable("id") int id) {
		return sous.getsousbyag(id);
	}

	@GetMapping("/getsousbycat/{id}")
	public List<Souscription> getsousbycat(@PathVariable("id") int id) {
		return sous.getsousbycat(id);

	}

	@GetMapping("/getsouscriptionbydate/{date1}/{date2}")
	public List<Souscription> getsouscriptionbydate(@PathVariable("date1") String date1,
			@PathVariable("date2") String date2) {
		// System.out.println(test.toString());
		return (List<Souscription>) sous.getsouscriptionbydate(date1, date2);
	}

	@GetMapping("/getsouscriptionbydateandcat/{id}")
	public List<Souscription> getsouscriptionbydateandcat(@RequestBody Souscription x, @PathVariable("id") int id) {
		return sous.getsouscriptionbydateandcat(x.getDate(), id);
	}

	@GetMapping("/getsousbyaganddate/{id}")
	List<Souscription> getsousbyaganddate(@PathVariable("id") int id, @RequestBody Souscription x) {
		return sous.getsousbyaganddate(id, x.getDate());
	}

	@GetMapping("/getsousbyagandcat/{id}/{id2}")
	public List<Souscription> getsousbyagandcat(@PathVariable("id") int id, @PathVariable("id2") int id2) {
		return (List<Souscription>) sous.getsousbyagandcat(id, id2);
	}

	@PutMapping("/valider/{id}")
	public ResponseEntity<Souscription> valider(@PathVariable("id") int id, @RequestBody Souscription Sous) {
		Random rand = new Random();
		ProdSous ps = new ProdSous();
		Carte c = new Carte();
		Cheque ch = new Cheque();
		Compte_a_vue cpav = new Compte_a_vue();
		Banqueadistance bq = new Banqueadistance();
		Compteépargne cae = new Compteépargne();
		int rand_int1 = rand.nextInt(19999);
		System.out.println("Update type ID = " + id + "...");
		Optional<Souscription> sousData = sous.findById(id);
		if (sousData.isPresent()) {
			Souscription souss = sousData.get();
			souss.setCl(souss.getCl());
			souss.setCp(souss.getCp());
			souss.setDate(souss.getDate());
			souss.setLibelle(souss.getLibelle());
			souss.setNumSouscription(souss.getNumSouscription());
			souss.setProd(souss.getProd());
			souss.setEtat("valider");
			ps.setLibelle(souss.getLibelle());
			ps.setProd(souss.getProd());
			ps.setDate(souss.getDate());
			ps.setSous(souss);
			prodsous.save(ps);
			if ((souss.getEtat().equals("valider")) && (souss.getProd().getType().getLibelle().equals("carte"))) {
				c.setCl(souss.getCl());
				c.setCp(souss.getCp());
				c.setNum(rand_int1);
				c.setProds(ps);
				carte.save(c);
			}
			if ((souss.getEtat().equals("valider")) && (souss.getProd().getType().getLibelle().equals("chequiers"))) {
				ch.setCl(souss.getCl());

				ch.setProds(ps);
				cheque.save(ch);
			}
			if ((souss.getEtat().equals("valider"))
					&& (souss.getProd().getType().getLibelle().equals("Souscription Package"))) {
				c.setCl(souss.getCl());
				c.setCp(souss.getCp());
				c.setNum(rand_int1);
				c.setProds(ps);
				ch.setProds(ps);
				ch.setCl(souss.getCl());
				cheque.save(ch);
				carte.save(c);
			}
			if ((souss.getEtat().equals("valider")) && (souss.getProd().getType().getLibelle().equals("Ecopage"))) {
				cae.setCl(souss.getCl());
				cae.setCp(souss.getCp());
				cae.setNum(rand_int1);
				cae.setProds(ps);
				Ce.save(cae);

			}
			if ((souss.getEtat().equals("valider")) && (souss.getProd().getType().getLibelle().equals("Facilité de caisse"))) {
				cpav.setCl(souss.getCl());
				cpav.setCp(souss.getCp());
				cpav.setNum(rand_int1);
				cpav.setProds(ps);
				cav.save(cpav);
			}
			if ((souss.getEtat().equals("valider"))
					&& (souss.getProd().getType().getLibelle().equals("banque a distance"))) {
				bq.setCl(souss.getCl());
				bq.setCp(souss.getCp());
				bq.setNum(rand_int1);
				bq.setProds(ps);
				bad.save(bq);

			}

			return new ResponseEntity<>(sous.save(souss), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	 @GetMapping("/nombrevalider")
	 public Long nbrsousv() {
	 return sous.getsousbyetatvalider();
	 }@GetMapping("/nbrnonvalider")
	 public Long nbrnonvalider(){
	return sous.getsousbyetatnonvalider();

	}
	@GetMapping("/getsousbyetatvalider")
	public List<Souscription> souscriptionvalider() {
		return (List<Souscription>) sous.souscriptionvalider();
	}

	@GetMapping("/getsousbyetatnonvalider")
	public List<Souscription> souscriptionnonvalider() {
		return (List<Souscription>) sous.souscriptionnonvalider();
	}
	@GetMapping("/souscriptionnonvaliderag/{id}")
	public List<Souscription> souscriptionnonvaliderag(@PathVariable("id") int id){
		return (List<Souscription>) sous.souscriptionnonvaliderag(id);
	}
	@GetMapping("/souscriptionvaliderag/{id}")
	public List<Souscription> souscriptionvaliderag(@PathVariable("id") int id){
		return (List<Souscription>) sous.souscriptionvaliderag(id);
	}
	@GetMapping("/souscriptionvaliderag/{date1}/{date2}/{id}")
	public List<Souscription> souscriptiondateag(@PathVariable("date1") String d1,@PathVariable("date2") String d2,@PathVariable("id") int id){
		return (List<Souscription>) sous.souscriptiondateag(d1,d2,id);
	}
	@GetMapping("/getsousbycatanddate/{id}/{d1}/{d2}/")
	public List<Souscription> getsousbycatanddate(@PathVariable("id")int id,@PathVariable("d1")String d1,@PathVariable("d2")String d2){
		return (List<Souscription>) sous.getsousbycatanddate(id,d1,d2);
	}
	
	
	

	
	
		
	

}
