package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ItypeCompte;
import tn.essat.model.TypeCompte;

@CrossOrigin("*")
@RestController
@RequestMapping("/typecompte")
public class Resttypecompte {
	@Autowired
	private ItypeCompte tc;

	public void setTc(ItypeCompte tc) {
		this.tc = tc;
	}
	@GetMapping("/typecp")
	public List<TypeCompte>getall(){
		return ( List<TypeCompte>)tc.findAll();}
	
	@PostMapping("/save")
	public void save(@RequestBody TypeCompte t){
		tc.save(t);
		
		
		
	}
	@GetMapping("/count")
	public Long count() {
		return tc.count();
		
		
	}
	@DeleteMapping("/delctype/{id}")
	public void del(@PathVariable("id") int id){
		tc.deleteById(id);
		
		
		
	}
	@GetMapping("/gettype/{id}")
	public Optional<TypeCompte> gettypebyid(@PathVariable("id") int id){
		return tc.findById(id);
		
		
		
	}
	@PutMapping("/updatetcp/{id}")
	 
	  public ResponseEntity<TypeCompte> updateUtcp(@PathVariable("id") int id, @RequestBody TypeCompte Tcp) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<TypeCompte> tcpData = tc.findById(id);
	    if (tcpData.isPresent()) {
	    	TypeCompte tcpp = tcpData.get();
	    tcpp.setLibelle(Tcp.getLibelle());
	      
	     return new ResponseEntity<>(tc.save(tcpp), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	

}
