package tn.essat.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICatDao;
import tn.essat.Repository.IClientDao;
import tn.essat.Repository.IProdsousDao;
import tn.essat.model.Categorie;
import tn.essat.model.Client;
import tn.essat.model.Produit;
import tn.essat.model.prcent;



@CrossOrigin("*")
@RestController
@RequestMapping("/clt")

public class RestClt {

	@Autowired
	IClientDao cl;
	@Autowired
	ICatDao cat ;
	@Autowired
	IProdsousDao iProdsousDao;
	
	public void setCat(ICatDao cat) {
		this.cat = cat;
	}

	public void setCl(IClientDao cl) {
		this.cl = cl;
	}

	@GetMapping("/clients")
	public List<Client> getall() {
		return (List<Client>) cl.findAll();

	}
	@GetMapping("/clt/{id}")
	public Optional<Client>getcltbyid(@PathVariable("id") int id){
		
		return cl.findById(id);
		
	}

	@DeleteMapping("/delclt/{id}")
	public void delclt(@PathVariable("id") int id) {
		cl.deleteById(id);
	}

	@PostMapping("/addclt")
	public void addclt(@RequestBody Client c) {
		cl.save(c);
	}
	@GetMapping("/count")
	public long count() {
		return cl.count();
		
	}
	@PutMapping("/updateclt/{id}")
	 
	  public ResponseEntity<Client> updateUclt(@PathVariable("id") int id, @RequestBody Client Clt) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Client> CltData = cl.findById(id);
	    if (CltData.isPresent()) {
	    	Client cltt = CltData.get();
	     cltt.setNom(Clt.getNom());
	     cltt.setAdresse(Clt.getAdresse());
	     cltt.setCat(Clt.getCat());
	     cltt.setDate_creation(Clt.getDate_creation());
	     cltt.setAg(Clt.getAg());
	     
	      
	     return new ResponseEntity<>(cl.save(cltt), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	@GetMapping("/cltcat/{id}")
	public List<Client> getaalclientbycat(@PathVariable("id") int id){
		
		return (List<Client>)cl.getallClientBycat(id);
		
		
		
	}@GetMapping("/cltag/{id}")
	public List <Client>getallclientByagence(@PathVariable("id")int id){
		return (List<Client>)cl.getallclientByagence(id);
	}
	@GetMapping("countt/{id}")
	public Long getcountbycat(@PathVariable("id")int x) {
		return cl.countt(x);
	}
	
	@GetMapping("/ff")
	public List<prcent> porcentage(){
		List<Categorie> prcentagesP= (List<Categorie>) cat.findAll();
		List<prcent> prcentages=new ArrayList<prcent>();
		for (Categorie p : prcentagesP) {
			prcent pr = new prcent(p.getId(), this.getcountbycat(p.getId())*100/this.count());
			System.out.println(pr.toString());
			prcentages.add(pr);
			System.out.println(prcentages.toString());
		}
		System.out.println(prcentages.toString());
		return prcentages;
		
	}
	@GetMapping("test")
	public Client meilleur() {
		Long c = null;
		Client cl = null;
		List<Client> clients=this.getall();
		for (Client clt : clients) {
			Long x = iProdsousDao.cccl(clt.getId());
			if(c == null || c < x) {
				c = x;
				cl = clt;
			}
		}
		return cl;
	}@GetMapping("counttag/{id}")
	public Long counttag(@PathVariable("id")int x) {
		return cl.counttag(x);}
	@GetMapping("test2/{id}")
	public Client meilleurag(@PathVariable("id")int id) {
		Long c = null;
		Client cl = null;
		List<Client> clients=this.getallclientByagence(id);
		for (Client clt : clients) {
			Long x = iProdsousDao.cccl(clt.getId());
			if(c == null || c < x) {
				c = x;
				cl = clt;
			}
		}
		return cl;
	
	
	

}@GetMapping("/ff2/{id}")
public List<prcent> porcentage2(@PathVariable("id") int x){
	List<Categorie> prcentagesP= (List<Categorie>) cat.findAll();
	List<prcent> prcentages=new ArrayList<prcent>();
	for (Categorie p : prcentagesP) {
		prcent pr = new prcent(p.getId(), this.getcountbycatandag(x,p.getId())*100/this.counttag(x));
		System.out.println(pr.toString());
		prcentages.add(pr);
		System.out.println(prcentages.toString());
	}
	System.out.println(prcentages.toString());
	return prcentages;
	
}
@GetMapping("countcatag/{id1}/{id2}")
public Long getcountbycatandag(@PathVariable("id1")int x,@PathVariable("id2") int x2) {
	return cl.counttbyagandcat(x, x2);
}
	



}
