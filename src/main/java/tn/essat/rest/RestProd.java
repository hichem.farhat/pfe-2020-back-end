package tn.essat.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.IProdDao;
import tn.essat.Repository.IProdsousDao;
import tn.essat.model.Produit;


@CrossOrigin("*")
@RestController
@RequestMapping("/prod")

public class RestProd {
	@Autowired
	IProdDao prod;
	@Autowired
	IProdsousDao iProdsousDao;
	public void setProd(IProdDao prod) {
		this.prod = prod;
	}
	@GetMapping("test")
	public Produit meilleur() {
		Long c = null;
		Produit p = null;
		List<Produit> produits=this.getall();
		for (Produit produit : produits) {
			Long x = iProdsousDao.cc(produit.getId());
			if(c == null || c < x) {
				c = x;
				p = produit;
			}
		}
		return p;
	}
	@GetMapping("test2")
	public Produit meilleur2() {
		Long c = null;
		Produit p = null;
		List<Produit> produits=this.getall();
		for (Produit produit : produits) {
			Long x = iProdsousDao.cc(produit.getId());
			if(c == null || c < x) {
				c = x;
				p = produit;
			}
		}
		return p;
	}
	
	
	

	
	
	@GetMapping("/prods")
	public List<Produit> getall() {
		return (List<Produit>) prod.findAll();

	}

	@DeleteMapping("/delprod/{id}")
	public void delclt(@PathVariable("id") int id) {
		prod.deleteById(id);
	}

	@PostMapping("/addprod")
	public void addclt(@RequestBody Produit p) {
		prod.save(p);
	}
	@GetMapping("/count")
	public long count() {
		return prod.count();
		
	}
	@GetMapping("/getprod/{id}")
	public Optional <Produit> get(@PathVariable("id") int id) {
		return prod.findById(id);
	}
	@PutMapping("/updateprod/{id}")
	 
	  public ResponseEntity<Produit> updateUclt(@PathVariable("id") int id, @RequestBody Produit Prod) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Produit> prodData = prod.findById(id);
	    if (prodData.isPresent()) {
	    	Produit prodd = prodData.get();
	    prodd.setCodeprod(Prod.getCodeprod());
	    prodd.setDate_de_creation(Prod.getDate_de_creation());
	    prodd.setLibelle(Prod.getLibelle());
	    prodd.setType(Prod.getType());
	      
	     return new ResponseEntity<>(prod.save(prodd), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	@GetMapping("aze/{id}")
	public List <Produit>getbycar(@PathVariable("id")int x){
		return (List <Produit>) prod.getbycar(x);
		
		
	}
		
	}
	
	
	


