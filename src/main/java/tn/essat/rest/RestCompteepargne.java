package tn.essat.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICompteépargne;
import tn.essat.model.Banqueadistance;
import tn.essat.model.Compteépargne;
@CrossOrigin("*")
@RestController
@RequestMapping("/ce")

public class RestCompteepargne {
	@Autowired
	ICompteépargne ce;

	public void setCe(ICompteépargne ce) {
		this.ce = ce;
	}
	@GetMapping("getall")
	public List <Compteépargne> getall(){
		return (List <Compteépargne>) ce.findAll();}
	@DeleteMapping("delce/{id}")
	public void delete(@PathVariable("id")int id){
		 ce.deleteById(id);
		
		
	}
	@GetMapping("/count")
	public Long getcount() {
		return ce.count();
	}
	@GetMapping("/ceag/{id}")
	public List <Compteépargne> getallbyag(@PathVariable ("id") int id){
		return (List <Compteépargne>) ce.getallCompteépargneByagence(id);
	}
	@GetMapping("/countag/{id}")
	public Long countag(@PathVariable("id") int id) {
		return ce.countag(id);
	}
	

}
