package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICatDao;
import tn.essat.model.Categorie;


@CrossOrigin("*")
@RestController
@RequestMapping("/cat")

public class Restcat {
	@Autowired
	ICatDao cat;

	public void setCat(ICatDao cat) {
		this.cat = cat;
	}

	@PostMapping("/addcat")
	public void addcat(@RequestBody Categorie c) {
		cat.save(c);
	}

	@GetMapping("/allcats")
	public List<Categorie> getallcategorie() {
		return (List<Categorie>) cat.findAll();

	}

	@DeleteMapping("/delcat/{id}")
	public void delcat(@PathVariable("id") int id) {
		cat.deleteById(id);
	}
	@GetMapping("/count")
	public long count() {
		
		return cat.count();
		
	}
	@GetMapping("/cat/{id}")
	public Optional<Categorie>getcatbyid(@PathVariable("id") int id){
		
		return cat.findById(id);
		
	}
	@PutMapping("/updatecat/{id}")
	 
	  public ResponseEntity<Categorie> updatecat(@PathVariable("id") int id, @RequestBody Categorie CAt) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Categorie> CatData = cat.findById(id);
	    if (CatData.isPresent()) {
	    	Categorie catt = CatData.get();
	    catt.setNom(CAt.getNom());
	      
	     return new ResponseEntity<>(cat.save(catt), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	

}
