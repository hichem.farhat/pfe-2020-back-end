package tn.essat.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import tn.essat.Repository.ITypeprod;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.model.Typeprod;

import tn.essat.util.PDFGenerator;

@CrossOrigin("*")
@RestController
@RequestMapping("/typeprod")
public class Resttypeprod {
	@Autowired
ITypeprod type;
	@Autowired RestUser us;

	public void setType(ITypeprod type) {
		this.type = type;
	}
	@GetMapping("Typeprod")
	public List <Typeprod> gettall(){
		return (List <Typeprod>) type.findAll();
			
	}
	@PostMapping("/addtype")
	public void addtype(@RequestBody Typeprod t) {
		type.save(t);
	}
	@DeleteMapping("/deltype/{id}")
	public void deltype(@PathVariable("id") int id) {
		type.deleteById(id);
	}
	@GetMapping("/typeprod/{id}")
	public Optional<Typeprod>gettypebyid(@PathVariable("id") int id){
		
		return type.findById(id);
		
	}
	@PutMapping("/typeprod/{id}")
	 
	  public ResponseEntity<Typeprod> updatetypeprod(@PathVariable("id") int id, @RequestBody Typeprod Type) {
	    System.out.println("Update type ID = " + id + "...");
	     Optional<Typeprod> TypeData = type.findById(id);
	    if (TypeData.isPresent()) {
	      Typeprod typee = TypeData.get();
	     typee.setCode(Type.getCode());
	     typee.setLibelle(Type.getLibelle());
	      
	      
	      
	     return new ResponseEntity<>(type.save(typee), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    } }
	    @GetMapping("count")
	    Long count() {
	    	return type.count();
			
	    }
	    
	    @GetMapping(value = "/pdf",
	            produces = MediaType.APPLICATION_PDF_VALUE)
	    public ResponseEntity<InputStreamResource> customersReport() throws IOException {
	        List<Typeprod> types = (List<Typeprod>) type.findAll();
	 
	        ByteArrayInputStream bis = PDFGenerator.customerPDFReport(types);
	 
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename=types.pdf");
	 
	        return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.APPLICATION_PDF)
	                .body(new InputStreamResource(bis));
	    }

}
