package tn.essat.rest;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.IUserDao;
import tn.essat.Repository.Roledao;
import tn.essat.config.GestionToken;
import tn.essat.config.JwtRequest;
import tn.essat.config.JwtResponse;
import tn.essat.model.Agence;
import tn.essat.model.ROLE;
import tn.essat.model.Rolee;
import tn.essat.model.Typeprod;
import tn.essat.model.User;
import tn.essat.service.UserService;


@CrossOrigin("*")
@RestController
@RequestMapping(value = "/auth")
public class RestUser {
	@Autowired
    private GestionToken token_gen;

    @Autowired
    private UserService userService;
    @Autowired
    IUserDao us;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
@Autowired Roledao rolee;
  
	@Autowired
    private AuthenticationManager authenticationManager;
    @PostMapping(value = "/login")
    public JwtResponse signIn(@RequestBody JwtRequest request) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())  );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = userService.loadUserByUsername(request.getUsername());
        String token = token_gen.generateToken(user);
        JwtResponse response= new JwtResponse(token);
        
        return response;
    }
    @GetMapping("user")
	   // @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	    public Optional<User> getuser(@RequestHeader("Authorization") String authJWT){
	    	System.out.println(authJWT);
	    	int id = token_gen.getUserIdFromToken(authJWT);
	    	return us.findById(id);
	    }
    public void setToken_gen(GestionToken token_gen) {
		this.token_gen = token_gen;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public void setUs(IUserDao us) {
		this.us = us;
	}
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	public void setRolee(Roledao rolee) {
		this.rolee = rolee;
	}
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	@PostMapping("/save")
    public void add(@RequestBody User u){
    	//u.setRole(ROLE.USER);
    	
		
    	
    	u.setPassword(passwordEncoder.encode(u.getPassword()));
    	u.setRole(ROLE.USER);
    	us.save(u);
    	
    	
    }
    @GetMapping("/getall")
    public List <User> getall(){
    	return (List <User>) us.getus();
    	
    }
    @DeleteMapping ("/deleteuser/{id}")
    public void delete(@PathVariable("id")int id){
    	us.deleteById(id);
    	
    }
    @GetMapping("/getuser/{id}")
    public Optional<User> getuser(@PathVariable("id")int id){
    	return us.findById(id);
    	
    }
    @PutMapping("/updateuser/{id}")
	 
	  public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User Us) {
	   System.out.println("Update type ID = " + id + "...");
	   Optional<User> UsData = us.findById(id);
	
	   if (UsData.isPresent()) {
	    	User uss = UsData.get();
	    	uss.setName(Us.getName());
	    	
	    	uss.setUsername(Us.getUsername());
	    	uss.setPassword(passwordEncoder.encode(Us.getPassword()));
	    	
	    
	    	uss.setAg(Us.getAg());
	    	
	    	
	   
	      
	     return new ResponseEntity<>(us.save(uss), HttpStatus.OK);
	   } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	   } }
    //@PostMapping("/updateUser/")
    //void updateUser(@RequestBody User user) {
    	//us.updateUser(user.getUsername(),user.getAg(), user.getName(),user.getId());
    //}

    @PutMapping("/update/{id}")
	 
	  public ResponseEntity<User> Update(@PathVariable("id") int id, @RequestBody User Us) {
	   System.out.println("Update type ID = " + id + "...");
	   Optional<User> UsData = us.findById(id);
	
	   if (UsData.isPresent()) {
	    	User uss = UsData.get();
	    	uss.setName(Us.getName());
	    	
	    	uss.setUsername(Us.getUsername());
	    	uss.setPassword(Us.getPassword());
	    	
	    
	    	uss.setAg(Us.getAg());
	    	
	    	
	   
	      
	     return new ResponseEntity<>(us.save(uss), HttpStatus.OK);
	   } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	   } }
    @PutMapping("/updateprofile/{id}")
	 
	  public ResponseEntity<User> Updateprofile(@PathVariable("id") int id, @RequestBody User Us) {
	   System.out.println("Update type ID = " + id + "...");
	   Optional<User> UsData = us.findById(id);
	
	   if (UsData.isPresent()) {
	    	User uss = UsData.get();
	    	uss.setName(Us.getName());
	    	
	    	uss.setUsername(Us.getUsername());
	    	uss.setPassword(Us.getPassword());
	    	
	    
	    	uss.setAg(Us.getAg());
	    	
	    	
	   
	      
	     return new ResponseEntity<>(us.save(uss), HttpStatus.OK);
	   } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	   } }
    @PostMapping("/updateprofileee/")
	 
    public void updddddd(@RequestBody User u){
    	//u.setRole(ROLE.USER);
    	
    	
		u.setPassword(u.getPassword());
    	
    	
    	
    	us.save(u);
    	
    	
    }
    @PostMapping("/updatepass/")
	 
    public void upddddddpass(@RequestBody User u){
    	//u.setRole(ROLE.USER);
    	
    	
		u.setPassword(passwordEncoder.encode(u.getPassword()));
    	
    	
    	
    	us.save(u);
    	
    	
    }
  
   

}
