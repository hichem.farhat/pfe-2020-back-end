package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICreditDao;
import tn.essat.model.Credit;


@CrossOrigin("*")
@RestController
@RequestMapping("/credit")
public class RestCredit {
	@Autowired
	ICreditDao cp;

	public void setCp(ICreditDao cp) {
		this.cp = cp;
	} 
	@GetMapping("Credits")
	List <Credit> getall(){
		return (List <Credit>) cp.findAll();}


@PostMapping("save")
public void save(@RequestBody Credit c ) {
	c.setValidation("non");
	cp.save(c);
}
@DeleteMapping("/delcredit/{id}")
public void delclt(@PathVariable("id") int id) {
	cp.deleteById(id);
}
@GetMapping("/cred/{id}")
public Optional<Credit>gettypebyid(@PathVariable("id") int id){
	
	return cp.findById(id);
	
}
@PutMapping("/creditv/{id}")

public ResponseEntity<Credit> updatetypeprod(@PathVariable("id") int id, @RequestBody Credit Creditt) {
    System.out.println("Update type ID = " + id + "...");
     Optional<Credit> creditData = cp.findById(id);
    if (creditData.isPresent()) {
    	Credit crt = creditData.get();
    	crt.setCl(crt.getCl());
    	crt.setCp(crt.getCp());
    	crt.setDate(crt.getDate());
    	crt.setMontant(crt.getMontant());
    	crt.setValidation("oui");
    	
      
      
      
     return new ResponseEntity<>(cp.save(crt), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } }
@PutMapping("/update/{id}")

public ResponseEntity<Credit> updatecredit(@PathVariable("id") int id, @RequestBody Credit Creditt) {
    System.out.println("Update type ID = " + id + "...");
     Optional<Credit> creditData = cp.findById(id);
    if (creditData.isPresent()) {
    	Credit crt = creditData.get();
    	crt.setCl(Creditt.getCl());
    	crt.setCp(Creditt.getCp());
    	crt.setDate(Creditt.getDate());
    	crt.setMontant(Creditt.getMontant());
    	crt.setValidation(Creditt.getValidation());
    	
    	
      
      
      
     return new ResponseEntity<>(cp.save(crt), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } }
@GetMapping("/oui")
public List<Credit> validationoui(String validation){
	validation="oui";
	return (List<Credit>)cp.findByValidation(validation);
	
}
@GetMapping("/getcreditbyag/{id}")
public List<Credit> getallcreditbyagence(@PathVariable("id")int id){
	return (List<Credit>)cp.getallcreditbyagence(id);}
@GetMapping("/count")
public Long getcount() {
	return cp.count();
}
@GetMapping("/non")
public List<Credit> validationnon(String validation){
	validation="non";
	return (List<Credit>)cp.findByValidation(validation);
	
}@GetMapping("/valider")
public Long countui() {
	return cp.countui();
}@GetMapping("/nonvalider")
public Long countnon() {
	return cp.countnon();
}
@GetMapping("/p1")
long getpourcent1() {
	return this.countui()*100/this.getcount();}
@GetMapping("/p2")
long getpourcent2() {
	return this.countnon()*100/this.getcount();}
@GetMapping("/countag/{id}")
public Long countag(@PathVariable("id") int id){
	return cp.countag(id);
}
@GetMapping("/countagandvalider/{id}")
public Long countagandvalider(@PathVariable("id") int id ){
	return cp.countagandvalider(id);}
@GetMapping("/countagandnonvalider/{id}")
public Long countagandnonvalider(@PathVariable("id") int id ){
	return cp.countagandnonvalider(id);}


@GetMapping("/pp1/{id}")
long getpourcentag(@PathVariable("id") int id ) {
	return this.countagandvalider(id)*100/this.countag(id);}
@GetMapping("/pp2/{id}")
long getpourcent2ag(@PathVariable("id") int id) {
	return this.countagandnonvalider(id)*100/this.countag(id);}



}
