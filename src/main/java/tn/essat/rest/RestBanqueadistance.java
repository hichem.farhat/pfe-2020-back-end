package tn.essat.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.IBanqueadistance;
import tn.essat.model.Banqueadistance;

@CrossOrigin("*")
@RestController
@RequestMapping("/bad")
public class RestBanqueadistance {
	@Autowired
	IBanqueadistance bad;
	
	public void setBad(IBanqueadistance bad) {
		this.bad = bad;
	}
	@GetMapping("/getall")
	public List <Banqueadistance> getall(){
		return (List <Banqueadistance>) bad.findAll();
	}
	@DeleteMapping("/delbad/{id}")
	public void delete(@PathVariable ("id") int id) {
		bad.deleteById(id);
		
		
	}
	@GetMapping("/count")
	public Long getcount() {
		return bad.count();
	}
	@GetMapping("/badag/{id}")
	public List <Banqueadistance> getallbyag(@PathVariable ("id") int id){
		return (List <Banqueadistance>) bad.getallBanqueadistanceByagence(id);
	}
	@GetMapping("/countag/{id}")
	public Long countag(@PathVariable("id") int id) {
		return bad.countag(id);
	}

}
