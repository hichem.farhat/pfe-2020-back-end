package tn.essat.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICompte_a_vue;
import tn.essat.model.Banqueadistance;
import tn.essat.model.Compte_a_vue;
@CrossOrigin("*")
@RestController
@RequestMapping("/cav")
public class Rest_Compte_a_vue {
	@Autowired
	ICompte_a_vue cav;

	public void setCav(ICompte_a_vue cav) {
		this.cav = cav;
	}
	@GetMapping("/all")
	public List <Compte_a_vue> getall(){
		return (List <Compte_a_vue>)cav.findAll();
		
	}
	@DeleteMapping("/delcav/{id}")
	public void delete(@PathVariable ("id") int id) {
		cav.deleteById(id);
		
		
	}
	@GetMapping("/count")
	public Long getcount() {
		return cav.count();
	}
	@GetMapping("/cavag/{id}")
	public List <Compte_a_vue> getallbyag(@PathVariable ("id") int id){
		return (List <Compte_a_vue>) cav.getallCompte_a_vueByagence(id);
	}
	@GetMapping("/countag/{id}")
	public Long countag(@PathVariable("id") int id) {
		return cav.countag(id);
	}
	

}
