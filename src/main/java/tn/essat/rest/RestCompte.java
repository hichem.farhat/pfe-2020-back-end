package tn.essat.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.essat.Repository.ICompteDao;

import tn.essat.model.Compte;

@CrossOrigin("*")
@RestController
@RequestMapping("/compte")
public class RestCompte {
	@Autowired
	ICompteDao cp;

	public void setCp(ICompteDao cp) {
		this.cp = cp;
	} 
	@GetMapping("comptes")
	List <Compte> getall(){
		return (List <Compte>) cp.findAll();}


@PostMapping("save")
public void save(@RequestBody Compte c ) {
	cp.save(c);
}
@DeleteMapping("/delcompte/{id}")
public void delclt(@PathVariable("id") int id) {
	cp.deleteById(id);
}
@GetMapping("/getcompte/{id}")
public Optional<Compte>  getcompte(@PathVariable("id") int id) {
	return cp.findById(id);
}
@GetMapping("/count")
public Long count(){
	return cp.count();}
@PutMapping("/updatecp/{id}")

public ResponseEntity<Compte> updatecompte(@PathVariable("id") int id, @RequestBody Compte Cp) {
  System.out.println("Update type ID = " + id + "...");
   Optional<Compte> CpData = cp.findById(id);
  if (CpData.isPresent()) {
	  Compte cpp = CpData.get();
  cpp.setCode(Cp.getCode());
  cpp.setCl(Cp.getCl());
  cpp.setCp(Cp.getCp());
  cpp.setDate(Cp.getDate());
  cpp.setLibelle(Cp.getLibelle());
    
   return new ResponseEntity<>(cp.save(cpp), HttpStatus.OK);
  } else {
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  } }
@GetMapping("/getallcpag/{id}")
public List <Compte> getallcomptebyag(@PathVariable("id")int x){
	return (List <Compte>)cp.getallcomptebyag(x);
}
}
